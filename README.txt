check_nameserver is designed to test responses from a nameserver for a given domain.

It's not designed to be fast, it's certainly not the only way of doing this,
nor is it the only thing that'll do this for you.

It has been written for my own use and outputs the data I require.

If you find it useful, that's cool.

There's no warranty. If it eats your cat or blocks your plumbing, that's not my fault :-)

There are some defaults; but they're not too useful.

Usage:

check_nameserver takes the following arguments (in any order, and can be omitted to take defaults):

  qtype=<soa|ns|a|aaaa|txt etc> (defaults to SOA)
  domain=<some.domain> (defaults to localdomain)
  ns=<target.nameserver> (defaults to 127.0.0.1)
  timeout=<some.integer.timeout> (defaults to 1s)
  dnssec=<0|1|yes|no|on|off> (defaults to off)
  sleep=<some value accepted by Time::HiRes' sleep replacement> (defaults to 1s)
